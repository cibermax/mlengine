﻿using Microsoft.ML;
using Microsoft.ML.AutoML;
using MLEngine.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace MLEngine.Models
{
    public class Listener
    {
        private static string IPadress;
        private static string Port;
        public ObservableCollection<MLmodel> MLmodels { get; set; }
        private MLContext mlContext = new MLContext();
        DataViewSchema modelInputSchema;

        private Dictionary<string, PredictionEngine<BarLoadBinary, BarLoadBinaryPrediction>> binaryDictionary = new Dictionary<string, PredictionEngine<BarLoadBinary, BarLoadBinaryPrediction>>();
        private Dictionary<string, PredictionEngine<BarLoadMulti, BarLoadMultiPrediction>> multiDictionary = new Dictionary<string, PredictionEngine<BarLoadMulti, BarLoadMultiPrediction>>();
        private Dictionary<string, PredictionEngine<BarLoadRegression, BarLoadRegressionPrediction>> regressionDictionary = new Dictionary<string, PredictionEngine<BarLoadRegression, BarLoadRegressionPrediction>>();
        private Dictionary<string, PredictionEngine<BarLoadRegression, BarLoadRegressionPrediction>> timeSeriesDictionary = new Dictionary<string, PredictionEngine<BarLoadRegression, BarLoadRegressionPrediction>>();

        public Listener (ObservableCollection<MLmodel> mlModels, string ip, string port)
        {
            MLmodels = mlModels;
            IPadress = ip;
            Port = port;

            try
            {
                foreach (var model in MLmodels)
                {
                    ITransformer loadedModel = mlContext.Model.Load(model.FileNameModel, out DataViewSchema modelInputSchema);

                    if (model.TitleModel == "Binary Classification")
                        binaryDictionary.Add(model.PostfixModel, mlContext.Model.CreatePredictionEngine<BarLoadBinary, BarLoadBinaryPrediction>(loadedModel));
                    if (model.TitleModel == "Multi-class Classification")
                        multiDictionary.Add(model.PostfixModel, mlContext.Model.CreatePredictionEngine<BarLoadMulti, BarLoadMultiPrediction>(loadedModel));
                    if (model.TitleModel == "Regression")
                        regressionDictionary.Add(model.PostfixModel, mlContext.Model.CreatePredictionEngine<BarLoadRegression, BarLoadRegressionPrediction>(loadedModel));
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.ToString()); }

            ipPoint = new IPEndPoint(IPAddress.Parse(IPadress), Convert.ToInt32(Port));
            
        }

        IPEndPoint ipPoint;
        Socket listenSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        private CancellationTokenSource m_Cts;
        private Thread m_Thread;
        private readonly object m_SyncObject = new object();

        public void Start()
        {
            lock (m_SyncObject)
            {
                if (m_Thread == null || !m_Thread.IsAlive)
                {
                    m_Cts = new CancellationTokenSource();
                    m_Thread = new Thread(() => Listen(m_Cts.Token))
                    {
                        IsBackground = true
                    };
                    m_Thread.Start();
                }
            }
        }

        public void Stop()
        {
            lock (m_SyncObject)
            {
                m_Cts.Cancel();
                listenSocket.Close();
            }
        }

        private void Listen(CancellationToken token)
        {
            listenSocket.Bind(ipPoint);
            listenSocket.Listen(10);

            while (!token.IsCancellationRequested)
            {
                try
                {
                    Socket handler = listenSocket.Accept();
                    StringBuilder builder = new StringBuilder();
                    int bytes = 0; // количество полученных байтов
                    byte[] data = new byte[1024]; // буфер для получаемых данных

                    do
                    {
                        bytes = handler.Receive(data);
                        builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                    }
                    while (handler.Available > 0);


                    string message = Convert.ToString(ServerDataPrediction(builder.ToString()));

                    data = Encoding.Unicode.GetBytes(message);
                    handler.Send(data);                   // этот метод возвращает ответ
                    // закрываем сокет
                    handler.Shutdown(SocketShutdown.Both);
                    handler.Close();

                }
                catch (SocketException)
                {
                }
            }
        }

        public string ServerDataPrediction(string request)
        {
            var barLoadBinary = new BarLoadBinary();
            var barLoadMulti = new BarLoadMulti();
            var barLoadRegression = new BarLoadRegression();

            List<string> words = request.Split(';').ToList();
            string checker = words.Last();
            words.RemoveAt(words.Count() - 1);

            if (binaryDictionary.ContainsKey(checker))
            {
                for (int i = 0; i < words.Count(); i++)
                {
                    typeof(BarLoadBinary).GetProperty($"col{i + 1}").SetValue(barLoadBinary, float.Parse(words[i]));
                }

                var prediction = binaryDictionary[checker].Predict(barLoadBinary);
                return string.Format("{0}", prediction.Prediction);
            }
            else if (multiDictionary.ContainsKey(checker))
            {
                for (int i = 0; i < words.Count(); i++)
                {
                    typeof(BarLoadMulti).GetProperty($"col{i + 1}").SetValue(barLoadMulti, float.Parse(words[i]));
                }

                var prediction = multiDictionary[checker].Predict(barLoadMulti);
                return string.Format("{0}", prediction.Prediction);
            }
            else if (regressionDictionary.ContainsKey(checker))
            {
                for (int i = 0; i < words.Count(); i++)
                {
                    typeof(BarLoadRegression).GetProperty($"col{i + 1}").SetValue(barLoadRegression, float.Parse(words[i]));
                }

                var prediction = regressionDictionary[checker].Predict(barLoadRegression);
                return string.Format("{0}", prediction.Score);
            }

            return "Something went wrong";
        }
    }
}
